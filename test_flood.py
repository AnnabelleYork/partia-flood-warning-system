"""Unit test for the flood module"""

from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level
from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list, update_water_levels
from random import uniform, randint

def test_stations_level_over_threshold():
  # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "station beta"
    coord = (-3.0, 7.0)
    typical_range = (-3, 3)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, typical_range, river, town)

    # Setting the current level of the water for this station equal to the max of typical range 
    # (ie, relative water level is 1)
    s.latest_level = 3

    list_station = [s]
    
    # Setting the tolerance under the relative water level 
    # (ie should retun name and relative water level)
    tol = 0.9
    assert stations_level_over_threshold(list_station, tol) == [('station beta', 1)]

    # Setting the tolerance over relative water level
    # (ie shouldn't return anything)
    tol = 1.1
    assert stations_level_over_threshold(list_station, tol) == []


def test_stations_highest_rel_level():

  # Build list of stations
  stations = build_station_list()
    
  update_water_levels(stations)
  
  # Generate 50 lists of the highest rel water and 
  # check that the lenght is always consistent with what is asked
  for i in range(50):
    N = randint(0, 200)
    assert (len(stations_highest_rel_level(stations, N))) == N
    i = i + 1