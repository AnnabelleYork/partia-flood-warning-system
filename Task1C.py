from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius

def run():
    """Requirements for Task 1C"""

    # Build list of stations
    stations = build_station_list()

    #Coordinates of Cambridge city center
    centre = (52.2053, 0.1218)
    r = 10 

    print("")
    print("Stations within {} km of {}: ".format(r, centre))
    print("")
    print(stations_within_radius(stations, centre, r))
   

if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    run()