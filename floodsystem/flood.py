"""
This module provides tools for analysing the potential floods
"""

from floodsystem.station import MonitoringStation
from floodsystem.utils import sorted_by_key

def stations_level_over_threshold(stations, tol):
    """Returns a list of tuples containng the name of the station and the latest relative water 
    # if it's over a certain tolerance"""

    list_stations_above_tol = []

    for station in stations:
        if MonitoringStation.relative_water_level(station) != None:
            if MonitoringStation.relative_water_level(station) >= tol:
                list_stations_above_tol.append((station.name, MonitoringStation.relative_water_level(station)))

    #  The returned list is sorted by the relative level in descending order
    list_stations_above_tol = sorted_by_key(list_stations_above_tol, 1)
    list_stations_above_tol.reverse()

    return list_stations_above_tol


def stations_highest_rel_level(stations, N):

    """This function returns a list (of tuples) which contains the name and relative water levels
    of the N stations with the highest water levels"""

    # Create empty list
    list_stations_rel_level = []

    # If the data is consistent add all stations to the list (name and relative water level)
    for station in stations:
        if MonitoringStation.relative_water_level(station) != None:
            list_stations_rel_level.append((station, MonitoringStation.relative_water_level(station)))

    # Sort this list to have the highest water levels first
    list_stations_rel_level = sorted_by_key(list_stations_rel_level, 1)
    list_stations_rel_level.reverse()

    list_stations_only = []

    for n in range(len(list_stations_rel_level)):
        list_stations_only.append(list_stations_rel_level[n][0])

    # Keep only the N first stations of the list
    list_stations_only = list_stations_only[:N]

    return list_stations_only
    
