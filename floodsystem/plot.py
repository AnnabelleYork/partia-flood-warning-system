
"""This module contains plotting functions
"""
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from floodsystem.analysis import polyfit
import numpy as np
import matplotlib

def plot_water_levels(station, dates, levels):
  """This function plots a graph of the water level for a MonitoringStation for given dates and levels"""

  # Create lists of same lenght as dates to plot the min and max of typical range on the same graph
  list_min_typical = []
  list_max_typical = []
  for _ in range(len(dates)):
    list_min_typical.append(station.typical_range[0])
    list_max_typical.append(station.typical_range[1])


  # Plot
  plt.plot(dates, levels, label="water level")
  plt.plot(dates, list_max_typical, label="max typical range")
  plt.plot(dates, list_min_typical, label="min typical range")

  # Add axis labels, add legend, rotate date labels and add plot title
  plt.legend()
  plt.xlabel('date')
  plt.ylabel('water level (m)')
  plt.xticks(rotation=45)
  plt.title(station.name)

  # Display plot
  plt.tight_layout()  # This makes sure plot does not cut off date labels

  plt.show()


def plot_water_level_with_fit(station, dates, levels, p):
  ''' plots '''
  #determine least-squares polynomial fit
  poly, d0= polyfit(dates,levels, p)   

  dates_as_floats = matplotlib.dates.date2num(dates)
 
  #plot best-fit polynomial
  d = np.linspace(dates_as_floats[0], dates_as_floats[-1], 30)
  plt.plot(d,poly(d-d0), label = 'polynomial fit')
  
  #plot real data 
  plot_water_levels(station, dates, levels)
  plt.show()
