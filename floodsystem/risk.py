from floodsystem.flood import stations_level_over_threshold
from floodsystem.analysis import polyfit
from floodsystem.datafetcher import fetch_measure_levels
import numpy as np
import matplotlib.dates
from datetime import datetime, timedelta
from floodsystem.geo import stations_within_radius
from collections import defaultdict

def at_risk_stations(stations):
    '''asseses risk of flooding at an individual station based on current level and trend, returns a dictionary mapping station names to risk points (0-6)'''

   #create dictionaries of stations and their risk level (assesed by points)
    station_points={}
    for station in stations:
        station_points[station.name]=0
    
    #look at current level
    for station in stations_level_over_threshold(stations, 0.5):
        #2 points if above typical high
        if station[1] > 1:
            station_points[station[0]] +=3
        #1 point if near typical high (0.8 of range)
        elif station[1] > 0.8:
            station_points[station[0]]+=2
        else:
            station_points[station[0]]+=1
        
    for station in stations:
        #stations nearer the lower end of the range (<0.5) need not be considered
        if station_points[station.name]>0:
             #look at  trend over past 2 weeks to 5th order
            dates, levels = fetch_measure_levels(station.measure_id, dt=timedelta(days=5))
            try:
                trend, offset = polyfit(dates, levels, 4)
                #determine gradient of trend at last sample
                derivative1= np.polyder(trend)
                last_date = matplotlib.dates.date2num(dates[-1])
                gradient = derivative1(last_date-offset)
            
                #if level is rising
                if gradient >0:
                    station_points[station.name]+=2

                    # if rate of increase is increasing (ie graph shows no signs of reaching plateau soon)
                    derivative2= np.polyder(trend, m=2)
                    if derivative2(last_date-offset)>0:
                        station_points[station.name]+=1
            except:
                #not enough data to calculate polynomial so can't use this station
                station_points[station.name]= -100
                pass
            
        
    return station_points


def at_risk_towns(stations):
    '''determines risk of flooding in each town based on nearby station risk. returns a dictionary mapping town name to a list containing average risk points and a description (low-severe)'''
    
    #get station risk
    stations_risk= at_risk_stations(stations)
    
    #create output dictionary 
    towns_with_risk=defaultdict(list)

    #create and populate dictionary mapping town names to a list of the form [coordinate, risk points] where coordinate is a coordinate of a river in that town
    towns=defaultdict(list)
    for station in stations:
        towns[station.town]=[station.coord, 0]

   #determine risk in each town
    for town, data in towns.items():
        near_stations=stations_within_radius(stations, data[0], 7)
        really_near_stations= stations_within_radius(stations, data[0], 5)
        
        #get average points for rivers within 7km radius 
        points1= average_risk(near_stations, stations_risk)
        
        #get average for rivers within 5km radius (for higher weighting)
        points2= average_risk(really_near_stations,stations_risk)
        

        data[1]= (points1+points2)/2
        
        towns_with_risk[town]= [data[1],assess_risk(data[1])]

    return towns_with_risk

        
def assess_risk(points):
    'taking number of risk points a town has, returns the appropriate level of risk as a string'
    if points >=5:
        risk= 'Severe'
    elif points >=3:
        risk = 'High'
    elif points >=1:
        risk = 'Moderate'
    else:
        risk= 'Low'
        
    return risk
        
def average_risk(near_stations, stations_risk):
    '''returns average of the risk points of the stations in a list (assumed to be the list of nearest stations to a town), discounting stations with invalid data (points<0) '''
    #initialise values
    points=0
    valid_stations= 0 #counts number of stations actually used in calculating avergae
    
    #
    for station in near_stations:
        if stations_risk[station]>0: #discount the -100s
            points += stations_risk[station]
            valid_stations+=1
        
    if valid_stations != 0:
        points/= valid_stations
    else :
        points = 0

    return points
        







    


