# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT

"""This module contains a collection of functions related to
geographical data.

"""

#from .utils import sorted_by_key  # noqa 
from collections import defaultdict
from floodsystem.utils import sorted_by_key  # noqa
from haversine import haversine # function used to compute diastance from coordinates


def rivers_with_station(stations):
    """Build and return a set (ie no repeated data) of rivers which contain stations,
    based off of the list of stations (of type MonitoringStation) provided. Each river is represented as a string.

    """
    rivers_repeated= []
    for station in stations:
        rivers_repeated.append(station.river)
    river_set= set(rivers_repeated)
    return river_set



def stations_by_river(stations):

    """ Build and return a dctionary, in which the key is the river name, mapped to a list of station objects.
    """
    #initialise dictionary 
    river_dict= defaultdict(list)

    #iterarte through stations, adding them to dictionary
    for station in stations:
        river_dict[station.river].append(station.name)

    return dict(river_dict)


def rivers_by_station_number(stations, N):

    """Build and return a list of tuples of the N rivers with the greatest number of monitoring stations (unless more than one has the same as the Nth river, 
    in which case they are all added). List is sorted in order of number of stations, starting with the greatest. 
    Output is a list with entries of the form: (river name, number of stations)
    """   
    #get dictionary of all rivers mapped to a list of stations
    rivers_dict = stations_by_river(stations)
    
    #create empty list
    rivers_list= []

    #populate list with river names and number of stations
    for river in rivers_dict:
        rivers_list.append( (river, len(rivers_dict[river])))
    
   #sort list by number of stations in descending order
    sorted_river_list= sorted_by_key(rivers_list, 1, reverse=True)


    #set minimum number of stations required to be output, based on the Nth entity in the list
    if N<= len(sorted_river_list):
        minimum= sorted_river_list[N-1][1]
    else:
        raise ValueError ('N is greater than total number of rivers')
    
    #create empty list
    shortened_list=[]
    
    #populate list, including all rivers (in order) with stations greater than or equal to the minimum number
    for river in sorted_river_list:
        if river[1] >= minimum:
            shortened_list.append(river)
        else:
            break
    
    return shortened_list



def stations_by_distance(stations, p):

    """ Given a list of station objects and a coordinate p, 
    returns a list of (station, distance) tuples, 
    where distance (float) is the distance of the station 
    (MonitoringStation) from the coordinate p. 
    The returned list is also sorted by distance. 
    The returned distances are in kilometers."""

    output_list = []
    distance_to_station = 0.
    
    for station in stations:

        # Compute distance between the station and point p
        distance_to_station = haversine(station.coord, p)

        # Create tuple sith station name and distance from p
        station_and_distance = (station.name, distance_to_station)

        # Add sation and distance from p to list
        output_list.append(station_and_distance)

    # Sorting the list by second entry in tuples, ie by distance   
    output_list = sorted_by_key(output_list, 1)
    
    return output_list


def stations_within_radius(stations, centre, r):

    """ Uses the function stations_by_distance to return a list of which stations are within
    a circle or radius r and specific center"""

    # Use the list sorted by distance of Task1B
    list_distance_centre = stations_by_distance(stations, centre)

    i = 0
    for i in range(len(stations)):
        if list_distance_centre[i][1] <= r: # if distance smaller then radius continue
            i = i + 1
        else: # if distance bigger than radius, cut list at this point
            list_distance_within_radius = list_distance_centre[:i]
            list_distance_within_radius = sorted_by_key(list_distance_within_radius, 0)
            
            list_name_only = [] # create a list that only contains the names of these stations
            for n in range(len(list_distance_within_radius)):
                list_name_only.append(list_distance_within_radius[n][0])

            return list_name_only
            
    


