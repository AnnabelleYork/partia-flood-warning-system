import matplotlib.dates, numpy as np

def polyfit(dates, levels, p):
    ''' computes a least-squares fit of a polynomial of degree p to water level data, returning polynomial function and the offset value as a float  '''

    #convert dates to floats
    d = matplotlib.dates.date2num(dates)
    p_coeff = np.polyfit(d - d[0], levels, p)

    poly = np.poly1d(p_coeff)

    return (poly, d[0])