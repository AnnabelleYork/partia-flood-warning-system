from floodsystem.risk import assess_risk, average_risk


def test_assess_risk():
    assert assess_risk(0.5)== 'Low'
    assert assess_risk(1) == 'Moderate'
    assert assess_risk(3) == 'High'
    assert assess_risk(5) == 'Severe'

def test_average_risk():

    fake_near_stations =['Central','South', 'East']
    fake_station_risks= {'Central': -96, 'East':1, 'West':3, 'South':4, 'North': -99}
    #central should be discounted, so only average east and south
    assert average_risk( fake_near_stations, fake_station_risks) ==2.5
    #check it can handle when all data is invalid
    fake_near_stations2=['Central', 'North']
    assert average_risk(fake_near_stations2,fake_station_risks) ==0
