#import plotly 
#plotly.tools.set_credentials_file(username='5Crazy', api_key='7hjoly2xmd3A0v4i4Pgl')

import plotly.plotly as py
import plotly.graph_objs as go

from IPython.display import IFrame
from IPython.display import display
display(IFrame('https://plot.ly/~5Crazy/4', width=700, height=350))


mapbox_access_token = 'pk.eyJ1IjoiNWNyYXp5IiwiYSI6ImNqcmx3eXh6NzA2ankzeXRvZGQ1aHlxcngifQ.nBVerEztZiqhX5vJWA6o0g'


from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius
from floodsystem.flood import stations_highest_rel_level

# Build list of stations
stations1 = build_station_list()

stations = stations_highest_rel_level(stations1, 5)


latitudes = []
longitudes = []
names = []
for station in stations:
    coordinates = [station.coord]
    latitudes.append(str(coordinates[0][0]))
    longitudes.append(str(coordinates[0][1]))
    names.append(station.name)

### Add text, name of stations and water level?
### Colour according to overlevel
### Add circle radius

data = [
    go.Scattermapbox(
        lat=latitudes,
        lon=longitudes,
        mode='markers',
        marker=dict(
            size=5
        ),
        text=names,
    )
]

layout = go.Layout(
    autosize=True,
    hovermode='closest',
    mapbox=dict(
        accesstoken=mapbox_access_token,
        bearing=0,
        center=dict(
            lat=52.5,
            lon=-1
        ),
        pitch=0,
        zoom=5
    ),
)

fig = dict(data=data, layout=layout)
py.iplot(fig, filename='Plot stations')