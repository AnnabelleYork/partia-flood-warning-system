import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.plot import plot_water_levels

def run():
    """Requirements for Task 2E"""

    # Build list of stations
    stations = build_station_list()
    # Update water levels (online connection required)

    update_water_levels(stations)

    # Create a list of the 5 stations with the currently highest relative water level
    suspicious_stations = stations_highest_rel_level(stations, 5)

    for station in suspicious_stations:
        # Fetch data for water level and the corresponding date over 10 days
        dt = 10
        dates, levels = fetch_measure_levels(
        station.measure_id, dt=timedelta(days=dt))
        # Plot the curve of these water level
        plot_water_levels(station, dates, levels)
        
    

if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")
    run()
