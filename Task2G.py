
from floodsystem.risk import at_risk_towns
from floodsystem.stationdata import build_station_list, update_water_levels

def run():
    stations = build_station_list()
    
    update_water_levels(stations)
    risks= at_risk_towns(stations)
    print(risks)
    #print('Cambridge risk: %s' %risks['Cambridge'])
    



if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")
    run()