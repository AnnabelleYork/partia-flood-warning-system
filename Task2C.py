from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.station import MonitoringStation

def run():
    """Requirements for Task 2C"""

    # Build list of stations
    stations = build_station_list()
    
    update_water_levels(stations)

    N = 10

    list_stations = stations_highest_rel_level(stations, N)
    list_stations_rel_level = []

    for station in list_stations:

        list_stations_rel_level.append((station.name, MonitoringStation.relative_water_level(station)))

    print(list_stations_rel_level)



if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System ***")
    run()
