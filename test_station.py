# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""Unit test for the station module"""

from floodsystem.station import MonitoringStation, inconsistent_typical_range_stations
from random import uniform


def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town

def test_inconsistent_typical_range_stations():

    # This function tests both the function inconsistent_typical_range_stations in station
    # and the method typical_range_consistent in MonitoringStation
    
    # Defining station parameters
    s_id = 'test-s-id'
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    #trange = ...     Defined later on (range of water level)
    river = "River X"
    town = "My Town"
    
    #Testing that the function returns the name of the station if the range of water level is undefined
    trange = None
    # Create a station from the previous parameters with trange unexistant
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    list_station = [s]
    assert inconsistent_typical_range_stations(list_station) == ['some station']

    # Testing that the function doesn't return the name of the station if the range of water level is defined
    trange = (uniform(-4, 0), uniform(0, 7))
    # Create a station from the previous parameters a random trange
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    list_station = [s]
    assert inconsistent_typical_range_stations(list_station) == []


def test_relative_water_level():
    
    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "station alpha"
    coord = (-2.0, 4.0)
    typical_range = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, typical_range, river, town)

    # Setting the current level of the water for this station equal to the max of typical range (ie, should return 1)
    s.latest_level = 3.4445
    assert MonitoringStation.relative_water_level(s) == 1

    # Setting the current level of the water for this station equal to the min of typical range (ie, should return 0)
    s.latest_level = -2.3
    assert MonitoringStation.relative_water_level(s) == 0

    # Setting the current level of the water for this station einexistent (ie, shouldn't return anything)
    s.latest_level = None
    assert MonitoringStation.relative_water_level(s) == None



