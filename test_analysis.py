from floodsystem.analysis import polyfit
import matplotlib.dates

def test_polyfit():
    test_vals= [100,101,102,103,104,105] #shift=100
    test_dates= matplotlib.dates.num2date(test_vals)
    test_levels= [-1,2,7,14,23,34] # x^2 +2x -1 (after shift)
    poly, shift= polyfit(test_dates,test_levels, 2)
    assert shift == 100
    assert poly.order ==2
    assert len(poly.c)==3
    assert round(poly.c[0],4) ==1
    assert round(poly.c[1],4) ==2
    assert round(poly.c[2],4) ==-1
