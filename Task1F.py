
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.stationdata import build_station_list
from floodsystem.utils import sorted_by_key  



def run():
    # Put code here that demonstrates functionality

    # Build list of stations
    stations = build_station_list()

    list_inconsistent_stations = inconsistent_typical_range_stations(stations)

    print("")
    print("List of stations with inconsistent typical range values:")
    print("")
    print(sorted_by_key(list_inconsistent_stations, 0))


if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")
    run()