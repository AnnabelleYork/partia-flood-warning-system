"""Unit test for the geo module"""

from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance,  stations_by_river, rivers_by_station_number, rivers_with_station, stations_within_radius
from floodsystem.utils import sorted_by_key
from random import uniform, randint


def test_stations_by_distance():

    # Build list of stations
    stations = build_station_list()

    # Test that the list is built/contains info
    p = (uniform(-90,90), uniform(-180,180))
    station_distance_list = stations_by_distance(stations, p)
    assert len(station_distance_list) > 0

    # Test that the computed distances are positive
    n = 0
    for n in range(len(stations)):
        assert station_distance_list[n][1] >= 0

def test_stations_within_radius():
    # Build list of stations
    stations = build_station_list()

    # Creating a function that generates a list of stations within radius with random radius as imput
    def generate_list():
        centre = (52.5, -1)
        r = uniform(0,1000)
        list_stations_within_radius = stations_within_radius(stations, centre, r)
        return list_stations_within_radius

    list_stations_within_radius = generate_list()
    count = 0

    while count == 0:
        if list_stations_within_radius is not None:
            count = 1
            for n in range(len(list_stations_within_radius)):
                # Check that the list only contains strings (the names of the stations)
                assert type(list_stations_within_radius[n]) == str

        # If the list is empty, maybe the radius is too small or the center really isolated, 
        # so just genrate a new one until it works
        else:
            list_stations_within_radius = generate_list()


def test_rivers_with_station():
    '''Tests data type of output of rivers_with_station'''
    
    stations= build_station_list()
    function_output= rivers_with_station(stations)
   
    #check output is a set
    assert type(function_output) == set and len(function_output) > 0

    #check each element in output is a string

    for river in function_output:
        assert type(river) == str


def test_rivers_by_station_number():
    stations = build_station_list()
    
    #range of N selected to ensure N is less than number of rivers (unless data is really weird)
    N= randint(1,50)
    function_output = rivers_by_station_number(stations,N)

    assert type(function_output) == list

    list_length= len(function_output)
    
    #check datatypes of each entry

    assert all(type(function_output[i])== tuple and type(function_output[i][0])== str and type(function_output[i][1])== int for i in range(list_length-1))
    
    #check list is sorted in reverse order by number of stations
    assert function_output == sorted_by_key(function_output, 1, reverse=True)
    
    #check list is long enough 
    assert list_length >= N
    
    #check last entries in list are the same if list is longer than N
    if list_length > N:
            for i in range(N,list_length):
                    assert function_output[i][1]== function_output[N-1][1]

def test_stations_by_river():
        stations= build_station_list()

        rivers_dict= stations_by_river(stations)
        
        assert type(rivers_dict)== dict
        assert len(rivers_dict) > 0

