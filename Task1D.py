
from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station, stations_by_river

def run():
    
    print('TESTING RIVERS_WITH_STATIONS')
    stations = build_station_list()
    all_rivers= rivers_with_station(stations)
    sorted_rivers= sorted(all_rivers)
    print('lenght= %s' %len(all_rivers))
    print (sorted_rivers[0:10])
    
    print('TESTING STATIONS_BY_RIVER')
    rivers_and_stations = stations_by_river(stations)
    aire_stations= sorted(rivers_and_stations['River Aire'])
    print(aire_stations)

    cam_stations= sorted(rivers_and_stations['River Cam'])
    print(cam_stations)

    thames_stations= sorted(rivers_and_stations['River Thames'])
    print(thames_stations)

if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()
